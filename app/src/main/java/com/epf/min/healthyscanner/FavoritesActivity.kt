package com.epf.min.healthyscanner

import FavoriteAdapter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.epf.min.healthyscanner.data.ProductDatabase
import com.epf.min.healthyscanner.model.Product
import kotlinx.android.synthetic.main.activity_favorites.*
import kotlinx.coroutines.runBlocking


class FavoritesActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorites)

        favorites_recyclerview.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    override fun onStart() {
        super.onStart()
        val database = Room
            .databaseBuilder(this, ProductDatabase::class.java, "db")
            .build()

        val productDAO = database.getProductDAO()

        var favorites:MutableList<Product> = mutableListOf<Product>()

        runBlocking {
            val products = productDAO.getAllProducts()
            for (product in products) {
                if (product.favorite) {
                    favorites.add(product)
                }
            }
            favorites_recyclerview.adapter = FavoriteAdapter(favorites)//L'adapter gère des View Holder. Chaque View holder contient une vue (ici une ligne avec client genre etc..)
        }
    }
}
