package com.epf.min.healthyscanner

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.epf.min.healthyscanner.data.ProductDatabase
import com.epf.min.healthyscanner.model.Code
import com.epf.min.healthyscanner.model.Product
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET


class HistoryActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        history_recyclerview.layoutManager = //On gère l'affichage du recycler view
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    override fun onStart() {
        super.onStart()
        val database = Room
            .databaseBuilder(this, ProductDatabase::class.java, "db")
            .build()

        val productDAO = database.getProductDAO()

        runBlocking {
            /*val product1 = Product(null,"NutelloFavorite","",listOf("noisette","chocolat","huile de palme"),"A", "https://static.openfoodfacts.org/images/products/301/762/042/1006/front_fr.225.200.jpg", "iuzfzhifiee",true)
            val product2 = Product(null,"Nutello","",listOf("noisette","chocolat","huile de palme"),"B", "https://static.openfoodfacts.org/images/products/301/762/042/1006/front_fr.225.200.jpg", "iuzfzhifiee",false)

            productDAO.addProduct(product1)
            productDAO.addProduct(product2)*/

            val products = productDAO.getAllProducts()
            history_recyclerview.adapter = ProductAdapter(products)//L'adapter gère des View Holder. Chaque View holder contient une vue (ici une ligne avec client genre etc..)
        }

        bouton_scanner.setOnClickListener{
            val intent = Intent(this, ScanActivity::class.java)
            var barrecode = 0
            startActivityForResult(intent, barrecode)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            val result = data?.getStringExtra("code")
            println("Nous récupérons  : " + result)
            val intent = Intent(this, AddProductActivity::class.java)
            val bundle = Bundle()
            bundle.putString("code", result)
            intent.putExtras(bundle)
            startActivity(intent)
        }
        if (resultCode == RESULT_CANCELED) {
            // Write your code if there's no result
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.go_statistics_action ->{
                val intent = Intent(this, StatActivity::class.java)
                startActivity(intent)
            }
            R.id.go_favorites_action ->{
                val intent = Intent(this, FavoritesActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

}




