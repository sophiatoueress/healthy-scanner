package com.epf.min.healthyscanner

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.bumptech.glide.Glide
import com.epf.min.healthyscanner.data.ProductDatabase
import com.epf.min.healthyscanner.model.Product
import kotlinx.android.synthetic.main.activity_product_details.*
import kotlinx.coroutines.runBlocking

class ProductDetailsActivity : AppCompatActivity() {
    lateinit var product: Product

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)

        product_ingredients_recyclerview.layoutManager = //On gère l'affichage du recycler view
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        //??????????????????????
        //supportActionBar?.setDisplayHomeAsUpEnabled(true)

        product = intent.getParcelableExtra<Product>("id")!!//récupérer l'intention qui m'a lancé, qui a lancé l'activité details

        product_details_name_textview.text = product.product_name

        product_details_nutriscore_textview.text = product.nutrition_grade_fr

        Glide.with(product_details_imageview)
            .load(product.image_front_url)
            .into(product_details_imageview);

        val ingredients = product.listdingredients
        product_ingredients_recyclerview.adapter =
            IngredientAdapter(ingredients)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail_product, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.delete_product_action -> {
                androidx.appcompat.app.AlertDialog.Builder(this)
                    .setTitle("Supprimer le produit")
                    .setMessage("Êtes-vous sûr de vouloir supprimer ce produit?")
                    .setPositiveButton("Oui") { _, _ ->
                        delete(product)
                        finish()
                        Toast.makeText(this, "Produit supprimé", Toast.LENGTH_SHORT).show()
                    }
                    .setNegativeButton("Non") { dialog, _ ->
                        dialog.dismiss()
                    }
                    .show()
            }
            R.id.add_favorite_product_action -> {
                println(product.favorite)
                when (product.favorite) {
                    true -> {
                        product.favorite = false
                        update(product)
                        finish()
                        Toast.makeText(this, "Produit supprimé des favoris", Toast.LENGTH_SHORT)
                            .show()
                    }
                    false -> {
                        product.favorite = true
                        update(product)
                        finish()
                        Toast.makeText(this, "Produit ajouté aux favoris", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun delete(product: Product) {
        val database = Room
            .databaseBuilder(this, ProductDatabase::class.java, "db")
            .build()
        val productDAO = database.getProductDAO()
        runBlocking {
            productDAO.deleteProduct(product)
        }
    }

    fun update(product: Product) {
        val database = Room
            .databaseBuilder(this, ProductDatabase::class.java, "db")
            .build()
        val productDAO = database.getProductDAO()
        runBlocking {
            productDAO.updateProduct(product)
        }
    }

}


