package com.epf.min.healthyscanner.data

import androidx.room.*
import com.epf.min.healthyscanner.model.Product

@Dao
interface ProductDAO {

    @Query("select * from products")
    suspend fun getAllProducts() : List<Product>//suspend car dans une co-routine

    @Insert
    suspend fun addProduct(product: Product)

    @Delete
    suspend fun deleteProduct(product: Product)

    @Update
    suspend fun updateProduct(product: Product)
}

