package com.epf.min.healthyscanner

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.ingredient_view.view.*

class IngredientAdapter(val ingredients: List<String>) : RecyclerView.Adapter<IngredientAdapter.IngredientViewHolder>() {
    class IngredientViewHolder(val ingredientView: View) :  RecyclerView.ViewHolder(ingredientView)

    override fun getItemCount() = ingredients.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)//objet qui transfrorme un composant layout (objet de type xml) en objet de type View
        val ingredientView : View =
            inflater.inflate(R.layout.ingredient_view,parent,false)//transformation du layout en type view
        return IngredientViewHolder(ingredientView)//associer le layout crée à une vie = une ligne produit ici (layout car interface utilisateur)
    }

    override fun onBindViewHolder(holder: IngredientViewHolder, position: Int) {//Pour chacune des vues on me donne la position de la vue et je remplis avec le produit concerné
        val ingredient: String = ingredients[position]//
        holder.ingredientView.ingredient_textview.text = "${ingredient}"
    }

}
