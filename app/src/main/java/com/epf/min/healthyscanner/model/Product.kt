package com.epf.min.healthyscanner.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "products")//Pour que ROOM reconnaisse cette classe et pour définir le nom de la table associée
@Parcelize
data class Product (
        @PrimaryKey(autoGenerate = true) val id: Long?,
        var product_name: String,
        val generic_name_fr_imported: String?,
        var listdingredients: List<String>,
        val nutrition_grade_fr: String,
        val image_front_url: String,
        val ingredients_text: String,
        var favorite:Boolean
) : Parcelable
