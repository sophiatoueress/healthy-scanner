package com.epf.min.healthyscanner

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.epf.min.healthyscanner.model.Product
import kotlinx.android.synthetic.main.product_view.view.*


class ProductAdapter(val products: List<Product>) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {//on paramètre l'adapter avec la classe qu'on vient de créer
    class ProductViewHolder(val productView: View) :  RecyclerView.ViewHolder(productView)//classe interne --> syntaxe équivalente à l'appel du constructeur du super

    override fun getItemCount() = products.size//L'adapteur fournit une méthode que le recyclerview va utiliser qui va lui dire cb d'éléments il y a à afficher

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)//objet qui transfrorme un composant layout (objet de type xml) en objet de type View
        val productView : View =
            inflater.inflate(R.layout.product_view,parent,false)//transformation du layout en type view
        return ProductViewHolder(productView)//associer le layout crée à une vie = une ligne produit ici (layout car interface utilisateur)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {//Pour chacune des vues on me donne la position de la vue et je remplis avec le produit concerné
        val product: Product = products[position]// <=> .get(position)
        holder.productView.product_textview.text =
            "${product.product_name}"

        Glide.with(holder.productView)
            .load(product.image_front_url)
            .into(holder.productView.product_imageview);

        holder.productView.setOnClickListener {//Faire en sorte que quand on clique sur une vue ca execute la details activity
            val intent = Intent(it.context, ProductDetailsActivity::class.java) //it est la vue this serait l'adapter
            intent.putExtra("id",product)
            it.context.startActivity(intent)
        }
    }

}

