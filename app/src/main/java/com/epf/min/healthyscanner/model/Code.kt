package com.epf.min.healthyscanner.model

//On met des val pour sécuriser et ne pas pouvoir redéfinir une propriété
class Code
    (
        val code: Long,                  //Code barre du produit
        val product: Product             //Tout le reste des infos du produit
    )
