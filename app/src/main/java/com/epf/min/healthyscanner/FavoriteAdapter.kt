import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.epf.min.healthyscanner.ProductDetailsActivity
import com.epf.min.healthyscanner.R
import com.epf.min.healthyscanner.model.Product
import kotlinx.android.synthetic.main.product_view.view.*

class FavoriteAdapter(val favorites: List<Product>) : RecyclerView.Adapter<FavoriteAdapter.ProductViewHolder>() {
class ProductViewHolder(val productView: View) :  RecyclerView.ViewHolder(productView)

    override fun getItemCount() = favorites.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)//objet qui transfrorme un composant layout (objet de type xml) en objet de type View
        val productView : View =
            inflater.inflate(R.layout.product_view,parent,false)//transformation du layout en type view
        return ProductViewHolder(productView)//associer le layout crée à une vie = une ligne produit ici (layout car interface utilisateur)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {//Pour chacune des vues on me donne la position de la vue et je remplis avec le produit concerné
        val favorite: Product = favorites[position]// <=> .get(position)
        holder.productView.product_textview.text =
            "${favorite.product_name}"

        Glide.with(holder.productView)
            .load(favorite.image_front_url)
            .into(holder.productView.product_imageview);

        holder.productView.setOnClickListener {//Faire en sorte que quand on clique sur une vue ca execute la details activity
            val intent = Intent(it.context, ProductDetailsActivity::class.java) //it est la vue this serait l'adapter
            intent.putExtra("id",favorite)
            it.context.startActivity(intent)
        }
    }

}