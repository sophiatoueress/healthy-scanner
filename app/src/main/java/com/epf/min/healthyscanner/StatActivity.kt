package com.epf.min.healthyscanner

import android.graphics.Color
import android.media.audiofx.AudioEffect
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import com.epf.min.healthyscanner.data.ProductDatabase
import com.epf.min.healthyscanner.model.Product
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import kotlinx.android.synthetic.main.activity_stat.*
import kotlinx.coroutines.runBlocking

class StatActivity : AppCompatActivity() {
    var percentileA:Float = 0F
    var percentileB:Float = 0F
    var percentileC:Float = 0F
    var percentileD:Float = 0F
    var percentileE:Float = 0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stat)
        if (getNutriscoresPercentiles()!=0){
            setPieChartData()
        }
    }

    fun getNutriscoresPercentiles():Int {
        var totalA:Int = 0
        var totalB:Int = 0
        var totalC:Int = 0
        var totalD:Int = 0
        var totalE:Int = 0
        var totalProducts = 0
        var products:List<Product>

        val database = Room
            .databaseBuilder(this, ProductDatabase::class.java, "db")
            .build()

        val productDAO = database.getProductDAO()

        runBlocking {
            products = productDAO.getAllProducts()
            totalProducts = products.size
        }

        if (totalProducts !=0){
            for (product in products) {
                when(product.nutrition_grade_fr){
                    "A" -> totalA+=1
                    "B" -> totalB+=1
                    "C" -> totalC+=1
                    "D" -> totalD+=1
                    "E" -> totalE+=1
                }
            }
            percentileA = (100*totalA/totalProducts).toFloat()
            percentileB = (100*totalB/totalProducts).toFloat()
            percentileC = (100*totalC/totalProducts).toFloat()
            percentileD = (100*totalD/totalProducts).toFloat()
            percentileE = (100*totalE/totalProducts).toFloat()
        }

        return totalProducts
    }

    fun setPieChartData() {
        val xvalues = ArrayList<String>()
        if (percentileA != 0f){
            xvalues.add("A")
        }
        if (percentileB != 0f){
            xvalues.add("B")
        }
        if (percentileC != 0f){
            xvalues.add("C")
        }
        if (percentileD != 0f){
            xvalues.add("D")
        }
        if (percentileE != 0f){
            xvalues.add("E")
        }

        val yvalues = ArrayList<Float>()
        if (percentileA != 0f){
            yvalues.add(percentileA)
        }
        if (percentileB != 0f){
            yvalues.add(percentileB)
        }
        if (percentileC != 0f){
            yvalues.add(percentileC)
        }
        if (percentileD != 0f){
            yvalues.add(percentileD)
        }
        if (percentileE != 0f){
            yvalues.add(percentileE)
        }

        val piechartentry = ArrayList<Entry>()

        for ((i,item) in yvalues.withIndex())
        {
            piechartentry.add(Entry(item,i))
        }

        val colors = ArrayList<Int>()
        colors.add(Color.GREEN)
        colors.add(Color.YELLOW)
        colors.add(Color.CYAN)
        colors.add(Color.MAGENTA)
        colors.add(Color.RED)

        val piedataset = PieDataSet(piechartentry, "Nutriscore in %")

        piedataset.colors = colors

        piedataset.sliceSpace = 3f

        val data = PieData(xvalues,piedataset)
        piechart.data = data

        data.setValueTextSize(15f)

        piechart.setDescriptionPosition(3f,3f)

        piechart.animateY(2000)
    }


}