package com.epf.min.healthyscanner

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.epf.min.healthyscanner.data.ProductDatabase
import com.epf.min.healthyscanner.model.Code
import com.epf.min.healthyscanner.model.Product
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET


class AddProductActivity : AppCompatActivity() {


    interface ProduitService {
        @GET(".JSON")
        fun recup_produit(): Call<Code>
    }



    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        val bundle = intent.extras
        var num_produit = ""

        if (bundle != null) {
            num_produit = bundle.getString("code").toString()
        }
        println("Le numéro récupéré avant d'ajouter un produit est : " + num_produit)


        val database = Room
            .databaseBuilder(this, ProductDatabase::class.java, "db")
            .build()

        val productDAO = database.getProductDAO()

        val url = "https://fr.openfoodfacts.org/api/v0/product/" + num_produit + "/"

        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        retrofit.create(ProduitService::class.java).recup_produit()
            .enqueue(object : Callback<Code> {
                override fun onFailure(call: Call<Code>, t: Throwable) {
                    println(t)
                    error("KO")
                }

                override fun onResponse(call: Call<Code>, response: Response<Code>)
                {
                    val resultat = response.body()
                    if (resultat != null) {
                        val produit =
                            resultat.product // Vu qu'on ne travaille que sur le product on se fiche de la classe résultat

                        produit.listdingredients = produit.ingredients_text.split(", ").toList()

                        println(
                            "Le produit scanné est le : ${num_produit} " +
                                    "\nSon nom est ${produit.product_name}" +
                                    "\nSon nutriscore est : ${produit.nutrition_grade_fr.toUpperCase()} ! " +
                                    "\nL'adresse de son image est ${produit.image_front_url}"
                        )
                        println("Ses ingrédients sont : ")
                        for (ingredient in produit.listdingredients) {
                            println(ingredient)
                        }


                        val produit_a_add = Product(
                            null,
                            "",
                            "",
                            produit.listdingredients,
                            produit.nutrition_grade_fr.toUpperCase(),
                            produit.image_front_url,
                            "",
                            false
                        )

                        if (produit.generic_name_fr_imported=="" || produit.generic_name_fr_imported==null)//C'est un produit où le nom francais n'existe pas
                        {
                            produit_a_add.product_name=produit.product_name
                        }
                        else
                        {
                            produit_a_add.product_name=produit.generic_name_fr_imported
                        }
                        runBlocking {
                            productDAO.addProduct(produit_a_add)
                        }
                        finish()
                        overridePendingTransition(0,0)
                        val intent = Intent(this@AddProductActivity, HistoryActivity::class.java)
                        startActivity(intent)
                        overridePendingTransition(0,0)
                    }
                }
            }
            )
    }


}

