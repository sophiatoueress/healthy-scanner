package com.epf.min.healthyscanner.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.epf.min.healthyscanner.model.Product
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


@Database(entities = [Product::class], version = 1)
@TypeConverters(IngredientsConverter::class) //on indique les classes servant de conversion que room peut venir récup
abstract class ProductDatabase : RoomDatabase() {
    abstract fun getProductDAO() : ProductDAO
}

class IngredientsConverter {
    @TypeConverter
    fun fromList(list: List<String>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun fromString(value: String): List<String> {
        val listType = object : TypeToken<List<String>>() {}.type
        return Gson().fromJson(value, listType)
    }
}
