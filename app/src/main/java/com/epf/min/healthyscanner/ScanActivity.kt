package com.epf.min.healthyscanner

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.CaptureActivity

class ScanActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        val scanner = IntentIntegrator(this)

        scanner.setDesiredBarcodeFormats(IntentIntegrator.PRODUCT_CODE_TYPES) //On ne veut scanner que des barrecodes de produit
        scanner.setBeepEnabled(false)                                         //On enleve ce beep de l'enfer
        scanner.setPrompt("Scannez votre produit")
        scanner.setCaptureActivity(CaptureActivityPortrait::class.java)       //Pour avoir le beep en vertical

        scanner.initiateScan()

    }


override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    if(resultCode == Activity.RESULT_OK)
    {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
                println("Print : Cancelled")
            }
            else
            {
                Toast.makeText(this, "Scanned: " + result.contents, Toast.LENGTH_LONG).show()
                println("Print : Scanned " + result.contents)

                val intent  = Intent()
                intent.putExtra("code", result.contents.toString())
                setResult(RESULT_OK, intent )
                finish()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
}


class CaptureActivityPortrait : CaptureActivity()